HSLU AITEC Bulb
========================

1) Installation on Ubuntu 14.10
----------------------------------

### Clone project

    git clone https://bitbucket.org/hslu/aitecbulb.git

### Install dependencies

    apt-get install php5 php5-curl curl
    cd /path/to/project/dir
    curl -sS https://getcomposer.org/installer | php

### Get depending php packages

    php composer.phar install

### Configure additional parameters
Composer asks you for some additional parameters.
You need to enter your database settings.

### Run application on integrated webserver

    php app/console server:run

    