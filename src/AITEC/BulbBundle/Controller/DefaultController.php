<?php

namespace AITEC\BulbBundle\Controller;

use DateTime;
use Doctrine\DBAL\Types\Type;
use Exception;
use Phue\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AITEC\BulbBundle\Helpers\ColorHelper;

class DefaultController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        try {
            $phueClient = new Client($this->container->getParameter('bulb.server'), $this->container->getParameter('bulb.username'));
            try {
                $bulbGroup1 = $phueClient->getGroups()[1];
            } catch (Exception $ex) {
                $bulbGroup1 = $this->getEmptyGroup();
            }
            try {
                $bulbGroup2 = $phueClient->getGroups()[2];
            } catch (Exception $ex) {
                $bulbGroup2 = $this->getEmptyGroup();
            }
        } catch (Exception $ex) {
            $bulbGroup1 = $this->getEmptyGroup();
            $bulbGroup2 = $this->getEmptyGroup();
        }


        $repository = $this->getDoctrine()
            ->getRepository('AITECBulbBundle:Log');

        $logs = $repository->findBy([], ['timestamp' => 'DESC']);

        $dateTo = new DateTime('tomorrow');
        $dateFrom = clone $dateTo;
        $dateFrom->modify('-1 days');

        $daysToDisplay = $this->container->getParameter('bulb.daysingraph');

        $countOfLastXDays = array();
        for ($i = 0; $i < $daysToDisplay; $i++) {
            $query = $repository->createQueryBuilder('l')
                ->select('count(l) as countlog')
                ->where('l.timestamp < :timestamp')
                ->andWhere('l.timestamp > :timestamp2')
                ->setParameter('timestamp', $dateTo, Type::DATETIME)
                ->setParameter('timestamp2', $dateFrom, Type::DATETIME)
                ->getQuery();

            $count = $query->getResult()[0]['countlog'];
            $dateTo->modify('-1 days');
            $dateFrom->modify('-1 days');

            $countOfLastXDays[$i] = (int)$count;
        }

        $countOfLastXDaysFinal = array();
        $maxCount = max($countOfLastXDays);
        for ($i = 0; $i < $daysToDisplay; $i++) {
            $countOfLastXDaysFinal[$i] = array();
            $countOfLastXDaysFinal[$i]['count'] = 100 / $maxCount * $countOfLastXDays[$i];
            $countOfLastXDaysFinal[$i]['date'] = (new DateTime('today'))->modify('-' . $i . ' days');
        }

        return $this->render('AITECBulbBundle:Default:index.html.twig', array('logs' => $logs, 'group1' => $bulbGroup1,
            'group2' => $bulbGroup2, 'lastXDays' => $countOfLastXDaysFinal));
    }

    private function getEmptyGroup() {
        $bulbGroup = array();
        $bulbGroup['Name'] = "";
        $bulbGroup['Hue'] = "";
        $bulbGroup['Saturation'] = "";
        $bulbGroup['Brightness'] = "";
        return $bulbGroup;
    }
}
