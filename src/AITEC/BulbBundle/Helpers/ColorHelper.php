<?php

namespace AITEC\BulbBundle\Helpers;

/**
 * Created by IntelliJ IDEA.
 * User: ramon
 * Date: 06.12.14
 * Time: 15:26
 */
class ColorHelper
{
    private $hue;
    private $saturation;
    private $brightness;
    private $rgb;


    public function setRGBValue($rgbValue)
    {
        $this->rgb = $rgbValue;
        // Strip out the leading hashtag if there
        if (substr($this->rgb, 0, 1) == "#") $color = substr($this->rgb, 1);
        // If we don't now have 6 digits, we have a problem
        if (strlen($this->rgb) != 6) return false;
        // Get our red, blue, and green values in fractional form
        $red = ((float)hexdec(substr($this->rgb, 0, 2))) / 255;
        $blue = ((float)hexdec(substr($this->rgb, 4, 2))) / 255;
        $green = ((float)hexdec(substr($this->rgb, 2, 2))) / 255;
        // we need these later
        $highest = max($red, $green, $blue);
        $diff = $highest - min($red, $green, $blue);
        // We want the light to be as bright as our brightest individual color.
        $brightness = $highest;
        // Calculate our saturation as well as values used in the hue formula
        if ($diff == 0) {
            $base = 0;
            $delta = 0;
            $saturation = 0;
        } else {
            $saturation = $diff / $highest;
            $base = 0;
            $delta = 0;
            switch ($highest) {
                case $red:
                    $base = 0;
                    $delta = ($green - $blue) / ($diff * 2);
                    break;
                case $green:
                    $base = 25500; // Pure green, per the documentation
                    $delta = ($blue - $red) / ($diff * 2);
                    break;
                case $blue:
                    $base = 46920; // Pure blue, per the documentation
                    $delta = ($red - $green) / ($diff * 2);
                    break;
            }
        }
        // Correction for red-dominant purples to a positive value above blue instead of a negative value below red
        if ($delta < 0) {
            $base = 46920;
            $delta = (1 + $delta);
        }
        // Determine the conversion value for our delta value
        if ($base < 2) $scaling = 25500; // red to green occupies 38.910505836% of Hue's color space
        elseif ($base < 4) $scaling = 21420; // green to blue occupies occupy 32.684824902% of Hue's color space
        else $scaling = 18615; // blue to red occupies 28.40466926% of Hue's color space
        // Determine our appropriately-scaled hue value
        $this->hue = (int)($base + ($delta * $scaling));
        // Scale up our brightness and saturation to the right units
        $this->saturation = (int)($saturation * 255);
        $this->brightness = (int)($brightness * 255);
        return true;
    }

    function setHueValue($H,$S,$V) {

        var_dump($H,$S,$V);

        $RGB = array();

        if($S == 0)
        {
            $R = $G = $B = $V * 255;
        }
        else
        {
            $var_H = $H * 6;
            $var_i = floor( $var_H );
            $var_1 = $V * ( 1 - $S );
            $var_2 = $V * ( 1 - $S * ( $var_H - $var_i ) );
            $var_3 = $V * ( 1 - $S * (1 - ( $var_H - $var_i ) ) );

            if       ($var_i == 0) { $var_R = $V     ; $var_G = $var_3  ; $var_B = $var_1 ; }
            else if  ($var_i == 1) { $var_R = $var_2 ; $var_G = $V      ; $var_B = $var_1 ; }
            else if  ($var_i == 2) { $var_R = $var_1 ; $var_G = $V      ; $var_B = $var_3 ; }
            else if  ($var_i == 3) { $var_R = $var_1 ; $var_G = $var_2  ; $var_B = $V     ; }
            else if  ($var_i == 4) { $var_R = $var_3 ; $var_G = $var_1  ; $var_B = $V     ; }
            else                   { $var_R = $V     ; $var_G = $var_1  ; $var_B = $var_2 ; }

            $R = $var_R * 255;
            $G = $var_G * 255;
            $B = $var_B * 255;
        }

        $RGB['R'] = $R;
        $RGB['G'] = $G;
        $RGB['B'] = $B;

        return $RGB;

    }

    /**
     * @return mixed
     */
    public function getHue()
    {
        return $this->hue;
    }

    /**
     * @param mixed $hue
     */
    public function setHue($hue)
    {
        $this->hue = $hue;
    }

    /**
     * @return mixed
     */
    public function getSaturation()
    {
        return $this->saturation;
    }

    /**
     * @param mixed $saturation
     */
    public function setSaturation($saturation)
    {
        $this->saturation = $saturation;
    }

    /**
     * @return mixed
     */
    public function getBrightness()
    {
        return $this->brightness;
    }

    /**
     * @param mixed $brightness
     */
    public function setBrightness($brightness)
    {
        $this->brightness = $brightness;
    }


    /**
     * @return mixed
     */
    public function getRgb()
    {
        return $this->rgb;
    }

    /**
     * @param mixed $rgb
     */
    public function setRgb($rgb)
    {
        $this->rgb = $rgb;
    }

}